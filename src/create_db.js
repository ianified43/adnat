 var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "!Q2w3e4r"
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
  con.query("CREATE DATABASE adnat02", function (err, result) {
    if (err) throw err;
    console.log("Database created");
  });
});

var con2 = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "!Q2w3e4r",
  database: "adnat02"
});

con2.connect(function(err) {
  if (err) throw err;
  var sql = "CREATE TABLE `organisations` (`id` int(11) NOT NULL AUTO_INCREMENT, `name` varchar(50) NOT NULL, `hourly_rate` decimal(10,2) NOT NULL,  PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

  con2.query(sql, function (err, result) {
    if (err) throw err;
    console.log("Table organisations created");
  });

});

con2.connect(function(err) {
  if (err) throw err;
  var sql = "CREATE TABLE `users` (`id` int(11) NOT NULL AUTO_INCREMENT,`organisation_id` int(11) NOT NULL,`name` varchar(60) NOT NULL,`email_address` varchar(60) NOT NULL,`password` text NOT NULL, PRIMARY KEY (`id`), FOREIGN KEY (organisation_id) REFERENCES organisation(id) ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

  con2.query(sql, function (err, result) {
    if (err) throw err;
    console.log("Table users created");
  });

});

con2.connect(function(err) {
  if (err) throw err;
  var sql = "CREATE TABLE `shifts` (`id` int(11) NOT NULL AUTO_INCREMENT,`user_id` int(11) NOT NULL,`start_date` datetime NOT NULL,`finish_date` datetime NOT NULL,`break_length` int(3) NOT NULL,PRIMARY KEY (`id`),FOREIGN KEY (user_id) REFERENCES users(id)) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

  con2.query(sql, function (err, result) {
    if (err) throw err;
    console.log("Table shifts created");
  });

});
