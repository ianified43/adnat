var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "!Q2w3e4r",
  database: "adnat"
});

con.connect(function(err) {
  if (err) throw err;
  var sql = "CREATE TABLE `shifts` (`id` int(11) NOT NULL AUTO_INCREMENT,`user_id` int(11) NOT NULL,`start_date` datetime NOT NULL,`finish_date` datetime NOT NULL,`break_length` int(3) NOT NULL,PRIMARY KEY (`id`),FOREIGN KEY (user_id) REFERENCES users(id)) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("Table shifts created");
  });

});